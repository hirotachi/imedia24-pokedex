import PokemonCard from "../components/PokemonCard";
import styles from "@modules/Home.module.scss";
import { NextPage } from "next";
import DetailsModal from "@components/DetailsModal";
import { useDispatch, useSelector } from "react-redux";
import { AppState, wrapper } from "@/store/store";
import { getPokemonList } from "@/store/reducers/pokemonSlice";
import { useEffect } from "react";
import Head from "next/head";

const pokemon = Array.from({ length: 10 }).map((_, i) => ({
  id: i + 1,
  name: Math.random().toString(36).substring(7),
}));

type HomeProps = {
  pokemon: {
    id: number;
    name: string;
  }[];
};

const Home: NextPage<HomeProps> = () => {
  const { selected, loadingDetails, details, list, loadingList } = useSelector(
    (state: AppState) => state.pokemon
  );
  const dispatch = useDispatch();
  useEffect(() => {
    const listener = (e: any) => {
      //    get current scroll progress
      const scrollProgress =
        (window.scrollY / (document.body.scrollHeight - window.innerHeight)) *
        100;
      if (scrollProgress > 98 && !loadingList) {
        const nextPage = list.length / 20 + 1;
        dispatch(getPokemonList(nextPage));
      }
    };

    window.addEventListener("scroll", listener);
    return () => {
      window.removeEventListener("scroll", listener);
    };
  }, [list]);
  return (
    <>
      <Head>
        <title>Pokedex</title>
      </Head>
      <div className={styles.home}>
        {details && <DetailsModal details={details} />}
        <p className={styles.intro}>Discover millions of pokemons</p>
        <div className={styles.list}>
          {list.map((p) => {
            const id = parseInt(p.url.match(/pokemon\/(\d+)\//)?.[1] || "0");
            return (
              <PokemonCard
                key={id}
                {...p}
                id={id}
                loading={selected === id && loadingDetails}
              />
            );
          })}
        </div>
        {loadingList && <p className={styles.loading}>Loading...</p>}
      </div>
    </>
  );
};
export default Home;

export const getServerSideProps = wrapper.getServerSideProps(
  (store) => async () => {
    await store.dispatch(getPokemonList(1));
    return {
      props: {
        pokemon: store.getState().pokemon,
      },
    };
  }
);
