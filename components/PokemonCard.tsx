import React, { FunctionComponent, useMemo } from "react";
import styles from "@modules/PokemonCard.module.scss";
import useHover from "@hooks/useHover";
import { useDispatch } from "react-redux";
import { getPokemonDetails } from "@/store/reducers/pokemonSlice";

type PokemonCardProps = {
  id: number;
  name: string;
  loading?: boolean;
};
const PokemonCard: FunctionComponent<PokemonCardProps> = (props) => {
  const dispatch = useDispatch();
  const { id, name, loading } = props;
  const hover = useHover();
  const img = useMemo(() => {
    return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/versions/generation-i/red-blue${
      hover.isHovered ? "" : "/gray"
    }/${id}.png`;
  }, [id, hover.isHovered]);
  const selectPokemon = () => {
    dispatch(getPokemonDetails(id));
  };
  return (
    <div className={styles.card} {...hover.props} onClick={selectPokemon}>
      {loading && (
        <span className={styles.load}>
          <ion-icon name="sync-circle-outline"></ion-icon>
        </span>
      )}
      <div className={styles.img}>
        <img src={img} alt={name} />
      </div>
      <p className={styles.name}>{name}</p>
    </div>
  );
};

export default PokemonCard;
