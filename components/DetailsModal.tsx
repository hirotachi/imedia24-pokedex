import React, { FunctionComponent } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  selectSelectedPokemonState,
  setSelectedPokemon,
} from "@/store/reducers/pokemonSlice";
import styles from "@modules/DetailsModal.module.scss";

type DetailsModalProps = {
  details: Pokemon;
};
const DetailsModal: FunctionComponent<DetailsModalProps> = (props) => {
  const { details } = props;
  const selectedPokemon = useSelector(selectSelectedPokemonState);
  const wantedConfig: (keyof Pokemon)[] = [
    "height",
    "weight",
    "base_experience",
    "abilities",
  ];

  const dispatch = useDispatch();
  const close = () => {
    if (!selectedPokemon) {
      return;
    }
    dispatch(setSelectedPokemon(undefined));
  };

  return (
    <div className={styles.details}>
      <div className={styles.wrapper}>
        <div className={styles.modal}>
          <span className={styles.modal__close} title={"close"} onClick={close}>
            <ion-icon name="close-outline" />
          </span>
          <div className={styles.container}>
            <div className={styles.main}>
              <div className={styles.header}>
                <p className={styles.name}>
                  {details.name}{" "}
                  <span className={styles.order}>
                    #{details.order?.toString().padStart(3, "0")}
                  </span>
                </p>
                <div className={styles.types}>
                  {details.types?.map((t) => (
                    <p key={t.type.name} className={styles.type}>
                      {t.type.name}
                    </p>
                  ))}
                </div>
              </div>
              <div className={styles.img}>
                <img
                  src={`https://unpkg.com/pokeapi-sprites@2.0.2/sprites/pokemon/other/dream-world/${details.id}.svg`}
                  alt={details.name}
                />
              </div>
            </div>
            <div className={styles.info}>
              <div className={styles.section}>
                <p className={styles.section__name}>About</p>
                <div className={styles.items}>
                  {wantedConfig.map((key) => {
                    let val = details[key];
                    if (key === "abilities") {
                      val = (val as Ability[])
                        .map((a) => a.ability.name)
                        .join(", ");
                    }
                    return (
                      <div className={styles.item} key={key}>
                        <p className={styles.label}>{key}</p>
                        <p className={styles.value}>{val as string}</p>
                      </div>
                    );
                  })}
                </div>
              </div>
              <div className={styles.section}>
                <p className={styles.section__name}>Stats</p>
                <div className={styles.stats}>
                  {details.stats?.map((stat) => {
                    return (
                      <div className={styles.stat} key={stat.stat.name}>
                        <p className={styles.stat__name}>{stat.stat.name}</p>
                        <span className={styles.stat__val}>
                          {stat.base_stat}
                        </span>
                        <div className={styles.stat__bar}>
                          <div
                            className={styles.stat__bar__inner}
                            style={{ width: `${stat.base_stat}%` }}
                          ></div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DetailsModal;
