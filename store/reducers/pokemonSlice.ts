import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { AppState } from "@/store/store";

export interface PokemonState {
  selected: number | null;
  loadingDetails: boolean;
  loadingList: boolean;
  details?: Pokemon | null;
  list: PokemonResult[];
}

const initialState: PokemonState = {
  selected: null,
  loadingDetails: false,
  loadingList: false,
  details: null,
  list: [],
};

export const getPokemonDetails = createAsyncThunk(
  "pokemon/getPokemonDetails",
  async (id: number, { dispatch }) => {
    dispatch(setSelectedPokemon(id));
    const response = await fetch(`https://pokeapi.co/api/v2/pokemon/${id}`);
    return response.json();
  }
);

export const getPokemonList = createAsyncThunk(
  "pokemon/getPokemonList",
  async (page: number) => {
    const response = await fetch(
      `https://pokeapi.co/api/v2/pokemon?limit=20&offset=${(page - 1) * 20}`
    );
    const data = await response.json();
    return data.results;
  }
);

export const pokemonSlice = createSlice({
  name: "pokemon",
  initialState,
  reducers: {
    setSelectedPokemon(state, action) {
      if (!action.payload) {
        state.selected = null;
        state.details = null;

        return;
      }
      state.selected = action.payload;
    },
  },
  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.pokemon,
      };
    },
    [getPokemonDetails.pending.type]: (state) => {
      state.loadingDetails = true;
    },
    [getPokemonDetails.fulfilled.type]: (state, action) => {
      state.loadingDetails = false;
      state.details = action.payload;
    },
    [getPokemonDetails.rejected.type]: (state) => {
      state.loadingDetails = false;
    },
    [getPokemonList.pending.type]: (state) => {
      state.loadingList = true;
    },
    [getPokemonList.fulfilled.type]: (state, action) => {
      if (!state.loadingList) return;
      state.loadingList = false;
      state.list = [...state.list, ...action.payload];
    },
    [getPokemonList.rejected.type]: (state) => {
      state.loadingList = false;
    },
  },
});

export const { setSelectedPokemon } = pokemonSlice.actions;
export const selectSelectedPokemonState = (state: AppState) =>
  state.pokemon.selected;
export default pokemonSlice.reducer;
