import { useState } from "react";

function useHover() {
  const [isHovered, setIsHovered] = useState(false);

  const onMouseOver = () => setIsHovered(true);
  const onMouseOut = () => setIsHovered(false);

  return { isHovered, setIsHovered, props: { onMouseOver, onMouseOut } };
}
export default useHover;
