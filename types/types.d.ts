type PokemonResult = {
  name: string;
  url: string;
};

interface HeldItem {
  item: {
    name: string;
    url: string;
  };
}

interface Move {
  move: {
    name: string;
    url: string;
  };
}

interface Stat {
  base_stat: number;
  effort: number;
  stat: {
    name: string;
    url: string;
  };
}

interface Type {
  slot: number;
  type: {
    name: string;
    url: string;
  };
}

type Pokemon = {
  id: number;
  name: string;
  order: number;
  abilities: Ability[];
  base_experience: number;
  forms: PokemonResult[];
  height: number;
  weight: number;
  held_items: HeldItem[];
  is_default: boolean;
  moves: Move[];
  stats: Stat[];
  types: Type[];
};

type Ability = {
  ability: {
    name: string;
    url: string;
  };
  is_hidden: boolean;
  slot: number;
};
